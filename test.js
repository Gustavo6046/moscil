const moscil = require('./index.js');

const baseFreq = 230; // C3

let tr = moscil.waves.triangle(baseFreq);
let sw = moscil.waves.sawtooth(baseFreq);

let trsw1 = moscil.mixers.adder(tr, sw).speed(0.999);
let trsw2a = moscil.mixers.adder(tr, sw);
let trsw2b = trsw2a.speed(1.001);

let trsw2 = moscil.mixers.adder(trsw2a, trsw2b);

let finalOscillator = moscil.mixers.modulators.ring(trsw2, trsw1);

let notes = [];

for (let x = -12; x <= 0; x++) notes.push(x);
notes = notes.map((x) => finalOscillator.note(x));

let i = 0;

function next() {
    if (i >= notes.length - 1)
        notes[i].play(4);

    else
        notes[i++].play(0.125).on('flush', () => next());
}

next(0);